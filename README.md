![Pipeline Status](https://gitlab.com/ekkebus/story-mapping/badges/master/pipeline.svg)
![coverage report](https://gitlab.com/ekkebus/story-mapping/badges/master/coverage.svg)

---

Project based on ['Xebia Story Mapping Game' by Serge Beaumont & Marco Mulder](https://xebia.com/storymappinggame) from Xebia and is released under Creative Commons Attribution 4.0 International (CC BY 4.0) https://creativecommons.org/licenses/by/4.0/

## Play now

👉 [`https://ekkebus.gitlab.io/story-mapping/`](https://ekkebus.gitlab.io/story-mapping/) 👈

