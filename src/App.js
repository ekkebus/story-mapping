import React, { Component } from 'react'
import { Route } from 'react-router'
import { Layout } from './components/Layout'
import Game from './components/Board'
import Footer from './components/Footer'
import './style/custom.css'
import './assets/css/FontAwesome-all.min.css'

export default class App extends Component {
    static displayName = App.name;

    render() {
        return (
            <Layout>
                <Route path='/' exact={true} component={Game} />
                <Footer />
            </Layout>
        );
    }
}
