import React from 'react'
import ReactDOM from 'react-dom'
import Board, { cardData } from '../components/Board'
import { mount, configure } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Settings from '../components/Settings'
import Slice from '../components/Slice'

configure({ adapter: new Adapter() });

it('renders without crashing', async () => {
    const div = document.createElement('div');
    ReactDOM.render(<Board />, div);
});

it('loads card data', async () => {
    expect(cardData).not.toBe(null)
});

describe('clicking on buttons', () => {
    it('opens card deck', async () => {
        const component = mount(
            <Board />, { attachTo: document.getElementById('root') }
        );
        component
            .find('#btn-card-deck-show')
            .simulate('click');
        expect(component).toMatchSnapshot();
        component.unmount();
    });

    it('set timer to true', async () => {
        const component = mount(
            <Board />, { attachTo: document.getElementById('root') }
        );
        component
            .find('#btn-card-deck-show')
            .simulate('click');
        expect('timer' in component.state()).toEqual(true);
        component.unmount();
    });

    //TODO only checks if you the button is clickable not if card deck is actually closed
    it('closes card deck', async () => {
        const component = mount(
            <Board />, { attachTo: document.getElementById('root') }
        );
        component
            .find('#btn-card-deck-close')
            .simulate('click');
        expect(component).toMatchSnapshot();
        component.unmount();
    });

    it('closes card deck on esc', async () => {
        const component = mount(
            <Board />, { attachTo: document.getElementById('root') }
        );
        component
            .find('#btn-card-deck-close')
            .simulate('keypress', { key: 'Escape' });
        expect(component).toMatchSnapshot();
        component.unmount();
    });

    it('adds slice', async () => {
        const component = mount(
            <Board />, { attachTo: document.getElementById('root') }
        );
        component
            .find('#btn-add-slice')
            .simulate('click');
        expect(component).toMatchSnapshot();
        expect(component.find(Slice).state().currentCount).toEqual(4);
        component.unmount();
    });

    it('adjusts cards to settings', async () => {
        const component = mount(
            <Settings />, { attachTo: document.getElementById('root') }
        );
        expect(component).toMatchSnapshot();
        expect(component.state().showCardStoryPoints).toBeDefined();
        expect(!component.state().showCardStoryPoints).toBe(component.state('cards').Settings['HideCardPoints']);
        component.unmount();
    });

    it('adjusts slice to setting', async () => {
        const component = mount(
            <Settings />, { attachTo: document.getElementById('root') }
        );
        expect(component).toMatchSnapshot();
        expect(component.state().showSliceStoryPoints).toBeDefined();
        expect(!component.state().showSliceStoryPoints).toBe(component.state('cards').Settings['HideSlicePoints']);
        component.unmount();
    });
});