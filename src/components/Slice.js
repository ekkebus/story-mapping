import React from 'react'
import DroppableSlice from './DroppableSlice'
import DraggableSlice from './DraggableSlice'
import { cardData } from './Board'
import { getCardHeaderColor } from './Card'

export default class Slice extends React.Component {
    constructor(props) {
        super(props);
        this.state = { currentCount: 4 };
        this.init = true;
        // eslint-disable-next-line no-func-assign
        sliceCounter = sliceCounter.bind(this);
    }

    /**
     * Draws all the slices including the folded slice
     * Does not draw the columns of the slice, see drawSliceParts
     */
    drawSlice() {
        let allSlices = [];
        for (let i = 0; i < this.state.currentCount; i++) {
            let className = 'slice-border-' + i;
            let sliceTitle = 'slice-title-' + i;
            let foldedSliceName = 'folded-' + sliceTitle;

            allSlices.push(
                <DroppableSlice key={'board-' + i} id={'board-' + i} className='board'>
                    <DraggableSlice key={'slice-' + i} id={'slice-' + i} className='noDropList list slice'>
                        <div className={'slice-menu-holder slice-' + i + '-menu'}>
                            <button key={'collapse-btn' + i} type='button' className='collapse-slice'
                                onClick={this.toggleSlice.bind(this, className, ('logo-' + i), sliceTitle)}><i
                                    id={'logo-' + i}
                                    className='fas fa-compress-arrows-alt' />
                            </button>
                        </div>
                        <div key={'slice-parent-' + i} id={'slice-parent'} className={className}>
                            <textarea spellCheck='false' id={sliceTitle} className='slice-name placeholder-owner'
                                onChange={(e) => this.handleTextAreaChange(e, foldedSliceName + '-text')}
                                textplaceholder='Enter title here' placeholder={'Enter title here'}
                                maxLength='250' autoFocus={!this.init} />
                            {this.drawSliceParts()}
                            <textarea spellCheck='false' id={sliceTitle + '-goal'}
                                className='slice-goal placeholder-owner'
                                onChange={(e) => this.handleTextAreaChange(e, foldedSliceName + '-goal')}
                                placeholder={'Enter goal here'} textplaceholder='Enter goal here' maxLength='250'>
                            </textarea>
                        </div>

                        <div key={'slice-' + i + '-storypoints'} id={'slice-' + i + '-storypoints'}
                            className='story-points-slice storypoints-display-slice'>
                        </div>

                        <div key={foldedSliceName} id={foldedSliceName} className={'folded-slice list noDropList'}
                            style={{ display: 'none' }}>
                            <textarea spellCheck='false' key={foldedSliceName + '-text'} id={foldedSliceName + '-text'}
                                className='slice-name' onChange={(e) => this.handleTextAreaChange(e, sliceTitle)}
                                placeholder={'Enter title here'} maxLength='250' />
                            <div key={foldedSliceName + '-card-count-border'} className={'card-count-border'}>
                                {this.renderSliceCardInformation(i)}
                            </div>
                            <textarea spellCheck='false' id={foldedSliceName + '-goal'}
                                className='slice-goal placeholder-owner'
                                placeholder={'Enter goal here'} textplaceholder='Enter goal here' maxLength='250'
                                onChange={(e) => this.handleTextAreaChange(e, sliceTitle + '-goal')}>
                            </textarea>
                        </div>
                        <div key={'slice-' + i + '-storypoints-2'} id={'slice-' + i + '-storypoints'}
                            className='story-points-slice storypoints-display-slice' />
                    </DraggableSlice>
                </DroppableSlice>
            )
        }
        this.init = false;
        return allSlices;
    }

    /**
     * Render slice information when the slice is folded.
     * This will be the placeholder of all the cards counted within a slice by column.
     *
     * @param sliceNumber The id of the slice inside your gameboard.
     * @returns {*} The element that should be rendered with information about the slice.
     */
    renderSliceCardInformation(sliceNumber) {
        return (
            cardData.Columns.map((cards) =>
                <div key={this.getCardFlatColor(cards) + '-folded-card-name'}
                    className={this.getCardFlatColor(cards) + ' folded-card-name'}>
                    <div key={cards.Name + '-cards'} className={'card-count'}>
                        <div id={this.getCardFlatColor(cards) + '-number-' + sliceNumber}
                            style={getCardHeaderColor(cards)}
                            className={'card-count-number'}>0 Cards
                        </div>
                    </div>
                </div>
            )
        )
    }

    /**
     * Handles the other hidden textarea in a slice.
     * A hidden textarea is the area of a goal and or name of the slice that is not shown
     * due to it being collapsed or expanded.
     *
     * @param event The onChange event that is being called.
     * @param nameId The id of the textarea that we want to manipulate.
     */
    handleTextAreaChange(event, nameId) {
        document.getElementById(nameId).value = event.target.value;
    }

    /**
     * Collapse or expand a slice.
     *
     * @param className The name of the full slice.
     * @param logoId A specific id for each icon to collapse the slice.
     * @param foldedSlice The name of the sliced when it is collapsed.
     */
    toggleSlice(className, logoId, foldedSlice) {
        if (document.getElementsByClassName(className) != null) {
            let element = document.getElementsByClassName(className);
            if (element[0].style.display !== 'none') {
                element[0].style.display = 'none';
                document.getElementById('folded-' + foldedSlice).style.display = 'flex';
                document.getElementById(logoId).className = 'fas fa-expand-arrows-alt';
            } else {
                element[0].style = '';
                document.getElementById(logoId).className = 'fas fa-compress-arrows-alt';
                document.getElementById('folded-' + foldedSlice).style.display = 'none'
            }
        }
    }
    /**
     * Draws all the columns of the slice
     * Column names are extracted from card set
     */
    drawSliceParts() {
        let sliceParts = [];
        cardData.Columns.map((cards) =>
            sliceParts.push(
                <DroppableSlice key={'slice-part-' + cards.Id} id={'slice-part-' + cards.Id}
                    className={'card' + this.getCardFlatColor(cards).toString().replace('#', '') + ' noDropList list SlicePart ' + this.getCardFlatColor(cards).toString().replace('#', '')}
                    columnColour={this.getCardFlatColor(cards)}>
                </DroppableSlice>
            )
        );
        return (sliceParts)
    }

    /**
     * @param cards The cards column json element.
     * @returns {*} The flat color from the color pallete assigned to this column.
     */
    getCardFlatColor(cards) {
        return cardData.Colors[cards.Colors]['Flat'];
    }

    render() {
        return (<div>{this.drawSlice()}</div>)
    }
}

export function sliceCounter() {
    this.setState({ currentCount: this.state.currentCount + 1 })
}