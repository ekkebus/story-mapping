import React from 'react'

export default class Timer extends React.Component {
    /**
     * two states are declared in the constructor: time and CountingUp
     * time decides the starting time (600 for 10 minutes). CountingUp
     * is used when the timer hits 0 to start counting up (see function UpdateTimer())
     * the function startTimer is bound here otherwise it starts running immediately
     * (instead of after looking at card deck)
     */
    constructor(props) {
        super(props);
        this.state = { time: 600, CountingUp: false };
        // eslint-disable-next-line no-func-assign
        startTimer = startTimer.bind(this);
        this.addMinute = this.addMinute.bind(this);
    }

    /**
     * This function is called by the button next to the timer on the webpage
     * calls the updateTimer function with extraMinutes = 1
     * (see comments at updateTimer)
     */
    addMinute() {
        let param = this;
        updateTimer(param, 1);
    }

    /**
     * standard render method that renders the components on the webpage
     * timer is by default set to display '10:00'
     */
    render() {
        return (<div className='timer-container'>
            <p id='timer' className='timer'>10:00</p>
            <i className='btn-add-minute fas fa-plus' onClick={this.addMinute} />
        </div>)
    }
}

/**
 * function that calls that initially starts the timer and calls the method again every second.
 */
export function startTimer() {
    let param = this;
    setInterval(function () {
        updateTimer(param)
    }, 1000);
}

/**
 * Updates the timer infinitely, goes negative after start time == 0
 * @param parameter reference to 'this'.
 * @param extraMinutes extra minutes added by a button click
 */
function updateTimer(parameter, extraMinutes = 0) {
    let timer, minutes, seconds;
    if (extraMinutes !== 0) {
        extraMinutes = 60 * extraMinutes;
        if (!parameter.state.CountingUp) {
            timer = parameter.state.time + extraMinutes;
        } else {
            if (parameter.state.time < extraMinutes) {
                document.getElementById('timer').style.color = 'white';
                parameter.setState({ CountingUp: false });
                timer = extraMinutes - parameter.state.time;
            } else {
                timer = parameter.state.time - extraMinutes;
            }
        }
    } else {
        timer = parameter.state.time
    }

    minutes = parseInt(timer / 60, 10);
    seconds = parseInt(timer % 60, 10);

    minutes = minutes < 10 ? '0' + minutes : minutes;
    seconds = seconds < 10 ? '0' + seconds : seconds;

    if (parameter.state.CountingUp) {
        if (extraMinutes === 0) {
            timer += 1;
        }
        document.getElementById('timer').textContent = '-' + minutes + ':' + seconds;
        document.getElementById('timer').style.color = 'red';
    } else {
        if (extraMinutes === 0) {
            timer -= 1;
        }

        if (timer <= 0) {
            parameter.setState({ CountingUp: true })
        }
        document.getElementById('timer').textContent = minutes + ':' + seconds;
        document.getElementById('timer').style.color = 'white';
    }
    parameter.setState({ time: timer });
}