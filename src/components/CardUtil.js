import queryString from 'query-string'

/**
 * Load a card set from a json file.
 * The type of cardset will be defined in the url of the game.
 * When no card set is found it will use a default one.
 */
export function loadCardData(props) {
    let cardData;
    let cardSetName = 'undefined';
    if (props.location !== undefined) {
        let parsedUrl = queryString.parse(props.location.search);
        cardSetName = parsedUrl['gameCards'];
    }
    if (typeof cardSetName === 'undefined') {
        // No card set was given, use the default one.
        cardData = require('../data/cardSets.json');
    } else {
        try {
            cardData = require('../data/' + cardSetName + '.json');
        } catch (ex) {
            // Card set did not exist, use the default card set.
            cardData = require('../data/cardSets.json');
        }
    }

    return cardData;
}