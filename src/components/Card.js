import React from 'react'
import DraggableDroppableCard from './DraggableDroppableCard'
import { cardData } from './Board'
import DroppableSlice from './DroppableSlice'

export default class Card extends React.Component {

    constructor(props) {
        super(props);
        this.cardZoom = this.cardZoom.bind(this);
        this.showCardNote = this.showCardNote.bind(this);
        this.setCardNote = this.setCardNote.bind(this);
        this.removeCardNote = this.removeCardNote.bind(this);
    }

    /**
     * Loops through all columns to find the cards inside of it.
     * Each card will be rendered individually.
     *
     * @see drawDroppable
     * @returns {*} The element that should be rendered.
     */
    drawCards() {
        return (
            <div key='wrapper' id='wrapper'>
                <div className='flex'>{cardData.Columns.map((cards) =>
                    <div key={cards.Id}>
                        {
                            this.drawDroppable(cards)
                        }
                    </div>
                )}
                </div>
            </div>
        )
    }

    /**
     * Renders a full cardset modal read from the json file.
     *
     * @param cards The section of cards from the json file.
     * @returns {*} The element that should be rendered.
     */
    drawDroppable(cards) {
        let columnData = this.getCardColumnData(cards.Id);
        let colorName = columnData['Colors']['Flat'].replace('#', '');
        let boardName = 'board-' + colorName;
        if (columnData == null) {
            return;
        }

        return (
            <DroppableSlice key={boardName}
                id={boardName}
                className={colorName + 'card list card-deck-board'}>
                <div className='card-deck-header'
                    style={getCardHeaderColor(cards)}>{cards.Name}</div>
                {
                    this.drawCardsInfo(cardData[cards.Colors + 'Cards'])
                }
            </DroppableSlice>
        );
    }

    /**
     * Get the column this card is a child from.
     * Every card has a column id to retrieve the parent column.
     *
     * @return {{Colors, ShortName: *, Id: *, Name: *}} The array from the parent column filled with data.
     */
    getCardColumnData(columnId) {
        let columns = cardData.Columns;
        for (let i = 0; i < columns.length; i++) {
            let columnValue = columns[i];
            if (columnValue.Id === columnId) {
                let colorName = columnValue.Colors;
                let colorScheme = cardData.Colors[colorName];
                return {
                    Id: columnId,
                    Name: columnValue.Name,
                    ShortName: columnValue.ShortName,
                    Colors: colorScheme,
                };
            }
        }
    }

    //Loops through all the cards so they can be drawn
    drawCardsInfo(cardDetail) {
        return (
            cardDetail.map((cards) => this.drawSingleCard(getCardData(cards), cards))
        )
    }

    //Draws all the cards one by one
    drawSingleCard(cardsData, cards) {
        return (
            <DraggableDroppableCard key={cardsData.text} id={cardsData.text}
                className={cardsData.color + ' card color' + cardsData.color}
                storypoints={String(cardsData.storyPoints)} colour={cardsData.color}>
                <div className='card-head' style={getCardHeaderColorFromCard(cards)}>
                    {/* modal for note */}
                    <div id={'note-overlay ' + cardsData.text} className='note-overlay' draggable={false}>
                        <div className='note-modal'>
                            <h2 className='note-header no-select'> Add note</h2>
                            <div className='note-body'>
                                <textarea spellCheck='false' className='note-textarea' rows='10' maxLength='1024' />
                            </div>
                            <div className='btn-note'>
                                <i className='far fa-trash-alt fa-2x btn-note-remove'
                                    onClick={this.removeCardNote} />
                                <i className='far fa-save fa-2x btn-note-save' onClick={this.setCardNote} />
                            </div>
                        </div>
                    </div>

                    <div className='card-head-text'>
                        {/* Makes the card bigger when clicked on zoom icon */}
                        <div id={'card-overlay ' + cardsData.text} className='card-overlay' onClick={this.cardZoomOff}>
                            <div className='card-modal' onClick={this.cardZoomOff}>
                                <div className='card-head' style={getCardHeaderColorFromCard(cards)}>
                                    {cardsData.name}
                                </div>
                                {/* text en storypoints of the card */}
                                <p className='card-zoom-popup'>{cardsData.text}</p>
                                <div className='card-foot' style={{ color: cardsData.color }}>
                                    {String(cardsData.storyPoints)}
                                </div>
                            </div>
                        </div>
                        {cardsData.name}

                        {/* buttons in the card header */}
                        <i onClickCapture={this.cardZoom} className='fas fa-search-plus card-zoom' />
                        <i onClickCapture={this.showCardNote} className='far fa-sticky-note' />
                    </div>
                </div>
                <div>
                    {/* text en storypoints of the card */}
                    <p>{cardsData.text}</p>
                    <div className='card-foot storypoints-display-cards' style={{ color: cardsData.color }}>
                        {String(cardsData.storyPoints)}
                    </div>
                </div>
            </DraggableDroppableCard>
        );
    }

    //Enlarge card text
    cardZoom(e) {
        let cardOverlay = e.currentTarget.closest('.card-head').querySelector('.card-overlay');
        cardOverlay.style.display = 'block';
    }

    cardZoomOff(e) {
        let cardOverlay = e.currentTarget.closest('.card-head').querySelector('.card-overlay');
        cardOverlay.style.display = 'none';
    }

    //get card note
    showCardNote(e) {
        e.preventDefault();
        let noteOverlay = e.currentTarget.closest('.card-head').querySelector('.note-overlay');
        noteOverlay.style.display = 'block';
    }

    //add new note to card
    setCardNote(e) {
        e.preventDefault();
        let noteOverlay = e.currentTarget.closest('.card-head').querySelector('.note-overlay');
        let newNote = noteOverlay.querySelector('.note-textarea').value;
        newNote !== '' ? noteOverlay.parentNode.querySelector('.card-head-text').querySelector('.fa-sticky-note').className = 'fas fa-sticky-note' : noteOverlay.parentNode.querySelector('.card-head-text').querySelector('.fa-sticky-note').className = 'far fa-sticky-note';
        e.currentTarget.closest('.note-overlay').style.display = 'none';
    }

    removeCardNote(e) {
        e.preventDefault();
        let noteOverlay = e.currentTarget.closest('.note-overlay');
        noteOverlay.querySelector('.note-textarea').value = '';
        noteOverlay.parentNode.querySelector('.card-head-text').querySelector('.fa-sticky-note').className = 'far fa-sticky-note';
        e.currentTarget.closest('.note-overlay').style.display = 'none';
    }

    render() {
        return (<div>{this.drawCards()}</div>)
    }
}

/**
 * Returns the column header from a card. Those color values
 * are stored inside the column so it requires its own method.
 *
 * @param jsonCardElement The card element in json.
 * @returns {*} The color of the specified column.
 */
function getCardHeaderColorFromCard(jsonCardElement) {
    let columnId = jsonCardElement.Column;
    let columnData = [];
    cardData.Columns.forEach(column => {
        if (column.Id === columnId) {
            columnData = getCardHeaderColor(column, false);
        }
    });

    return columnData;
}

/**
 * Gets all the known data assigned to a card from the cards json file.
 *
 * @param jsonElement The card element from the json file.
 * @return {*} The data of the card.
 */
function getCardData(jsonElement) {
    // Cards can have their own name, if they don't have one its being fallbacked to the column.
    let cardName = jsonElement.Name;

    let dataCard = {
        storyPoints: jsonElement.Storypoints === '?' ? '?' : parseInt(jsonElement.Storypoints),
        columnId: jsonElement.Column,
        text: jsonElement.Text
    };

    let columnName = null;
    let columnColor = null;

    cardData.Columns.forEach(column => {
        if (column.Id === jsonElement.Column) {
            if (typeof cardName === 'undefined') {
                columnName = column.ShortName;
            }
            let colorData = cardData.Colors[column.Colors];
            if (colorData === null) {
                columnColor = '#000'; // It's a black background, this means the color wasn't properly set up.
            } else {
                columnColor = colorData['Flat'];
            }
        }
    });

    dataCard.name = columnName;
    dataCard.color = columnColor;

    return dataCard;
}

/**
 * Get the background color from the card set file.
 * The background supports 2 gradient colors if set.
 *
 * @param jsonElement The json object of the cards columns.
 * @param useShadow Wheter the box should have a shadow.
 * @returns The styling element for the background of a column.
 */
export function getCardHeaderColor(jsonElement, useShadow = true) {
    // We always have a gradient and a flat color set for colors.
    let gradientColors = [];
    let flatColor;

    let colorData = cardData.Colors[jsonElement.Colors];
    if (colorData === null) {
        flatColor = '#000'; // It's a black background, this means the color wasn't properly set up.
    } else {
        flatColor = colorData['Flat'];
        gradientColors = colorData['Gradient'];
    }

    let styling = {
        background: flatColor,
        color: 'white',
    };

    if (useShadow) {
        styling.boxShadow = '1px 2px 2px rgba(3, 9, 12, 0.35)';
    }

    // We require 2 gradient colors to be set, if it's not two, we fallback to the original color as well.
    let length = gradientColors.length;
    if (length !== 2) {
        return styling;
    }

    styling.background = 'linear-gradient(' + gradientColors[0] + ' , ' + gradientColors[1] + ')'; // Gradient color for the cards.
    styling.backgroundColor = flatColor; // Default fallback for old browsers.
    return styling;
}