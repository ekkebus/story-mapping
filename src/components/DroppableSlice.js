import React from 'react'

export default class DroppableSlice extends React.Component {
    drop = (e) => {
        e.preventDefault();
        e.stopPropagation();
        const data = JSON.parse(e.dataTransfer.getData('text/plain'));
        const closestBoard = e.target.closest('.board');

        //Move slices
        if (data.id.includes('slice') && closestBoard != null) {
            let boardSlices = closestBoard.parentNode.childNodes;
            let sliceElement = [];
            let sliceArray = [];

            for (let i = 0; i < closestBoard.parentNode.childNodes.length; i++) {
                //get all slices in the board slices and put them in an array
                sliceElement = Array.prototype.slice.call(boardSlices[i].childNodes);
                sliceArray.push(sliceElement);
            }

            let targetSlice = closestBoard.childNodes;
            let indexRemove = getIndexOfArray(sliceArray, document.getElementById(data.id));
            let indexAdd = getIndexOfArray(sliceArray, targetSlice[0]);

            //remove the dragged slice
            sliceArray.splice(indexRemove[0], 1);
            //add the dragged slice in the right place
            sliceArray.splice(indexAdd[0], 0, [document.getElementById(data.id)]);

            //append the slice to the right board element
            for (let i = 0; i < sliceArray.length; i++) {
                let board = document.getElementById('board-' + i);
                board.appendChild(sliceArray[i][0]);
            }
        }
    };

    render() {
        return (<div id={this.props.id} columncolour={this.props.columnColour} onDrop={this.drop}
            className={this.props.className} style={this.props.style}>
            {this.props.children}
        </div>);
    }
}

/**
 * @param arr is the given array
 * @param k is the element we want to find
 * @return the index of the element given
 */
function getIndexOfArray(arr, k) {
    for (let i = 0; i < arr.length; i++) {
        let index = arr[i].indexOf(k);
        if (index > -1) {
            return [i];
        }
    }
}