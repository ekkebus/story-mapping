import React from 'react'
import '../style/footer.css'
import XebiaLogo from '../images/Xebia-logo.svg'

export default class Footer extends React.Component {
    render() {
        return (
            <footer className={'footer'}>
                <div className={'div'}>
                    <a className={'xebia'} href='https://xebia.com' target='_blank' rel='noopener noreferrer'
                        title='Xebia'>
                        <img src={XebiaLogo} alt={'Xebia logo'} width={50} />
                    </a>
                    Created by Serge Beaumont and Marco Mulder
                    <a className={'cc-license'} href='https://creativecommons.org/licenses/by/4.0/' target='_blank'
                        rel='noopener noreferrer' title='Creative Commons Attribution 4.0 International license'>
                        <i className='fab fa-creative-commons-by' />
                        <i className='fab fa-creative-commons' />
                    </a>
                </div>
            </footer>
        );
    }
}