import React from 'react'
import 'react-responsive-modal/styles.css'
import { Modal } from 'react-responsive-modal'
import Switch from 'react-switch'
import { loadCardData } from './CardUtil'

export default class Settings extends React.Component {
    constructor(props) {
        super(props);

        let cardData = loadCardData(props);

        let hideSlicePointsOnStart = cardData.Settings['HideSlicePoints'] === null ? true : cardData.Settings['HideSlicePoints'];
        let hideCardPointsOnStart = cardData.Settings['HideCardPoints'] === null ? true : cardData.Settings['HideCardPoints'];

        this.state = {
            cards: cardData,
            open: false,
            showSliceStoryPoints: !hideSlicePointsOnStart,
            showCardStoryPoints: !hideCardPointsOnStart,
        };
    }

    componentDidMount() {
        if (this.state.showSliceStoryPoints) {
            this.toggleDiv(false, 'storypoints-display-slice')
        }

        if (this.state.showCardStoryPoints) {
            this.toggleDiv(false, 'storypoints-display-cards')
        }
    }

    onOpenModal = () => {
        this.setState({ open: true });
    };

    onCloseModal = () => {
        this.setState({ open: false });
    };

    toggleSliceStoryPoints() {
        this.setState({ showSliceStoryPoints: !this.state.showSliceStoryPoints });
        this.toggleSlicePoints(this.state.showSliceStoryPoints);
    }

    toggleCardStoryPoints() {
        this.setState({ showCardStoryPoints: !this.state.showCardStoryPoints });
        this.toggleCardPoints(this.state.showCardStoryPoints);
    }

    render() {
        const { open } = this.state;
        return (
            <div key='settings' className={'rowC'}>
                <i className='fas fa-cog nav-icon hide-storypoint'
                    onClick={this.onOpenModal}>
                </i>
                <Modal open={open} closeOnEsc onClose={this.onCloseModal}>
                    <h2>Game Settings</h2>
                    <p>
                        Modify the game to your own needs.
                    </p>
                    {this.getSwitch('slicePoints', 'Show Slice Storypoints', this.toggleSliceStoryPoints.bind(this), this.state.showSliceStoryPoints)}
                    {this.getSwitch('cardPoints', 'Show Card Storypoints', this.toggleCardStoryPoints.bind(this), this.state.showCardStoryPoints)}
                </Modal>
            </div>
        );
    }

    getSwitch(key, txt, onChange, state) {
        let divs = [];
        let subDivs = [];
        subDivs.push(<span key={key + '-desc'}
            style={{ float: 'left', marginRight: 10 + 'px' }}>{txt}</span>);
        subDivs.push(
            <Switch
                key={key}
                onChange={onChange}
                checked={state}
                handleDiameter={10}
                height={20}
                width={48}
                className='react-switch'
                id='material-switch' />);
        divs.push(<div key={key + '-slider'} className={'settingType'} style={{ width: 100 + '%' }}> {subDivs} </div>);

        return divs;
    }

    toggleCardPoints(show) {
        // Hide card storypoints
        this.toggleDiv(show, 'storypoints-display-cards');
    }

    toggleSlicePoints(show) {
        // Hide slice storypoints
        this.toggleDiv(show, 'storypoints-display-slice');
    }

    toggleDiv(show, divName) {
        for (let e of document.getElementsByClassName(divName)) {
            e.style.display = !show ? 'block' : 'none';
        }
    }
}