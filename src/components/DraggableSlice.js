import React from 'react'

export default class DraggableSlice extends React.Component {
    drag = (e) => {
        let data = {
            'id': e.target.id,
            'colour': this.props.colour,
        };
        e.dataTransfer.setData('text/plain', JSON.stringify(data));
    }

    allowDrop = (e) => {
        e.preventDefault();
    };

    render() {
        return (<div key={this.props.id} id={this.props.id} draggable='true' className={this.props.className}
            onDragStart={this.drag} onDragOver={this.allowDrop}>
            {this.props.children}
        </div>);
    }
}
