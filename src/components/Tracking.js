import React, { Component } from 'react'
import { NotificationContainer, NotificationManager } from 'react-notifications'
import 'react-notifications/lib/notifications.css'

export default class Tracking extends Component {
    static displayName = Tracking.name;

    componentDidMount() {
        NotificationManager.info('This website may track your activity when adblocker & do not track are turned off');
    }

    render() {
        return (
            <div>
                <NotificationContainer />
            </div>
        );
    }
}
