import React from 'react'
import { cardData } from './Board'
import { getCardHeaderColor } from './Card'

export default class BoardHeader extends React.Component {

    render() {
        return (<div>{this.drawHeaders()}</div>)
    }

    /**
     * Draws the headers at the top of the playing field
     * Colors and names are from the card set
     */
    drawHeaders() {
        return (
            <div id='header' className='column-heads header scalable'>
                <div key='persona' id='persona' className='single-column-head margin-left Scalable single-column-head-background'>
                    <textarea spellCheck='false' id='persona-content' className='single-column-head-text placeholder-owner'
                        suppressContentEditableWarning={true} textplaceholder='Enter persona here'
                        placeholder='Enter persona here' />
                    <i onClick={this.addPersona} className='btn-add-persona far fa-image fa-2x' />
                </div>
                {
                    cardData.Columns.map((headers) =>
                        <div id={headers.Name.replace(' ', '-') + '-header'} key={headers.Name}
                            className='single-column-head scalable'
                            style={getCardHeaderColor(headers)}>
                            <h1 className='scalable'>{headers.Name}</h1>
                        </div>
                    )
                }
                <div key='customerJourneyGoal' id='customerJourneyGoal'
                    className='single-column-head margin-right single-column-head-background'>
                    <textarea spellCheck='false' id='customerJourneyGoal-content'
                        className='single-column-head-text placeholder-owner'
                        suppressContentEditableWarning={true} textplaceholder='Enter goal here'
                        placeholder='Enter goal here' />
                </div>
            </div>
        )
    }

    /**
     * Adds an image to the board above the headers
     * Only one image can be present
     */
    addPersona() {
        let link = prompt('Please enter image link, [gif, jpeg, png]', 'https://media.giphy.com/media/IfrfAy8zbHnPfUIWki/giphy.gif');
        if (link != null) {
            let img = document.createElement('img');
            img.src = link;
            img.id = 'persona-img';
            img.draggable = false;
            let persona = document.getElementById('persona-img-container');
            if (persona.children.length === 2) {
                persona.removeChild(document.getElementById('persona-img'))
            }
            persona.appendChild(img)
        }
    }
}

// When the user scrolls the page, execute stickyHeader
window.onscroll = function () { stickyHeader() };
// Add the sticky class to the header when you reach its scroll position. Remove 'sticky' when you leave the scroll position
function stickyHeader() {
    // Get the header
    let header = document.getElementById('header');
    if (window.pageYOffset > 200) {
        header.classList.add('sticky');
    } else {
        header.classList.remove('sticky');
    }
}