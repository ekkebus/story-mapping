import React, { Component } from 'react'
import { Nav, Navbar, NavbarBrand, NavbarText, NavItem } from 'reactstrap'
import '../style/NavMenu.css'
import screenfull from 'screenfull'
import Settings from './Settings'

export class NavMenu extends Component {
    static displayName = NavMenu.name;

    constructor(props) {
        super(props);

        this.toggleNavbar = this.toggleNavbar.bind(this);
        this.state = {
            isFull: false,
            collapsed: true,
            showStoryPoints: false,
        };
    }

    toggleNavbar() {
        this.setState({
            collapsed: !this.state.collapsed
        });
    }

    render() {
        return (
            <header>
                <div>
                    <Navbar expand='md' style={{
                        color: '#fff',
                        width: '100%',
                        background: 'rgb(23, 32, 42)',
                        borderBottom: '3px solid rgb(19, 23, 31)'
                    }}>

                        <NavbarBrand href='/story-mapping' style={{ color: '#fff', fontWeight: 'bold' }}>Story
                            Mapping</NavbarBrand>
                        <Nav className='mr-auto' navbar>
                            <NavItem />
                        </Nav>
                        <NavbarText>
                            <Settings />
                        </NavbarText>
                        <NavbarText>
                            <i className='fas fa-expand nav-icon rowC' onClick={this.goFullScreen} />
                        </NavbarText>
                    </Navbar>
                </div>
            </header>
        );
    }

    goFullScreen = () => {
        if (screenfull.isEnabled) {
            screenfull.toggle();
        } else {
            alert('Your browser does not support fullscreen.');
        }
    }
}