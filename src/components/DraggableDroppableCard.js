import React from 'react'

export default class DraggableDroppableCard extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            slice: '',
            slicePart: '',
            card: '',
            initialX: 0,
            initialY: 0,
            xOffset: 0,
            yOffset: 0,
            pickup: false,
            oldX: 0,
            color: '',
            oldSlice: '',
            isFirefox: navigator.userAgent.indexOf('Firefox') > -1,
            dragged: false,
        };
        this.click = this.click.bind(this);
        this.stop = this.stop.bind(this);
        this.drag = this.drag.bind(this);
        this.redo = this.redo.bind(this);
        this.leave = this.leave.bind(this);
        this.leaveCard = this.leaveCard.bind(this);
        this.offCard = this.offCard.bind(this);
    }

    /**
     * Called when the user clicks on a card.
     * This method will check what type of interaction happened
     * when clicked on the card.
     *
     * @param e The mousedown event.
     */
    click(e) {
        if (e.button === 0) {
            let isCardPlay = true;
            //Buttons on the card for zoom, note
            if (e.target.classList.contains('card-zoom') || e.target.classList.contains('fa-sticky-note') || e.target.closest('.card-overlay') || e.target.closest('.note-overlay')) {
                isCardPlay = false;
            }
            if (isCardPlay) {
                this.setState({ initialX: (e.clientX), initialY: (e.clientY) });

                let card = e.target.querySelector('.card');
                if (card == null) {
                    card = e.target.closest('.card');
                }

                //append to overlay and set the right coords
                if (card !== null) {
                    document.getElementById('deck-overlay-single').appendChild(card);
                    document.getElementById('deck-overlay').scrollTop = 0;
                    card.style.width = 16 + '%';
                    setTranslate(e.clientX - card.clientWidth / 2, e.clientY - card.clientHeight / 4, card, 'none');
                    updateStoryPoints();
                }

                let color = card.getAttribute('colour').replace('#', '');
                this.setState({ card: card, color: color });

                card.style.visibility = 'visible';
                card.addEventListener('mousemove', this.drag);
                card.addEventListener('mouseup', this.stop);
                card.addEventListener('mouseleave', this.redo);
                card.addEventListener('mouseleave', this.leaveCard);
                document.body.addEventListener('mouseleave', this.leave);
                document.getElementById('btn-card-deck-show').style.display = 'flex';
                document.getElementById('btn-card-deck-close').style.display = 'none';
                document.getElementById('deck-overlay').style.visibility = 'hidden';
                document.getElementById('deck-overlay').style.overflow = 'hidden';
                document.body.style.overflow = 'auto';

            }
        }
    }

    /**
     * Called when cursor leaves the body or cursor goes from card to certain element
     * appends the card to last slice it passed.
     * When no slice is given the card will be moved back to the card deck.
     *
     * @param e The mouseleave event.
     */
    leave(e) {
        let card = this.state.card;
        if (card !== '') {
            card.style.transform = 'none';
            card.style.width = 98 + '%';
            this.setState({ card: card, pickup: true });
            let oldSlice = this.state.oldSlice;
            if (oldSlice.id !== undefined) {
                oldSlice.querySelector('#' + this.state.slicePart.id).appendChild(card);
            }
            this.removeEvents();
            if (this.state.slice.id === undefined) {
                let selector = 'board-' + this.state.color;
                let card = this.state.card;
                card.style = '';
                document.getElementById(selector).appendChild(card);
                this.resetState();
            }
            updateStoryPoints();
            this.removeBackground(e);
            document.getElementById('btn-card-deck-show').style.display = 'flex';
            document.getElementById('btn-card-deck-redo').style.display = 'none';
        }
    }

    /**
     * Called when cursor leaves the card
     * Adds the event needed to track the card when cursor leaves the card
     */
    leaveCard(e) {
        if (e.relatedTarget.id.includes('feedback') || e.relatedTarget.className.includes('feedback')) {
            this.leave()
        } else {
            document.body.addEventListener('mousemove', this.offCard)
        }
    }

    /**
     * Called by mousemove when cursor is off the card but on the body
     * Necessary for a smooth drag experience
     *
     * @param e The mousemove event
     */
    offCard(e) {
        this.setCoords(e);
    }

    /**
     * Called when cursor is on the card and moving
     *
     * @param e The mouse move event.
     */
    drag(e) {
        document.body.removeEventListener('mousemove', this.offCard);
        document.getElementById('btn-card-deck-show').style.display = 'none';
        document.getElementById('btn-card-deck-redo').style.display = 'flex';
        this.setCoords(e);

        //Get all the slices and their coordinates, this is used to calculate the closest slice
        let slices = Array.from(document.querySelectorAll('.board')).reverse();
        let distanceSlices = [];
        let sliceCoords = slices.map(slice => {
            let rect = slice.getBoundingClientRect();
            return [rect.top];
        });

        sliceCoords.forEach(sliceCoord => {
            distanceSlices.push(sliceCoord)
        });

        function closestSlice(slice) {
            return slice <= e.clientY;
        }

        let closestSliceIndex = distanceSlices.findIndex(closestSlice);

        if (closestSliceIndex >= 0) {
            this.setState({ oldSlice: this.state.slice });
            this.setState({ slice: slices[closestSliceIndex] });
            this.setState({ slicePart: this.state.slice.querySelector('.card' + this.state.color) });
            this.setBackground();
        }
    }

    /**
     * Called when cursor goes from card to float button
     *
     * @param e The mouseout event.
     */
    redo(e) {
        if (e.relatedTarget !== null) {
            if (e.relatedTarget.id.includes('redo')) {
                this.backToDeck()
            }
        }
    }

    /**
     * Sets the highlights off the possible placements
     * Sets the highlight off the current selected placement
     */
    setBackground() {
        let color = this.state.color;
        if (color !== undefined) {
            let className = 'card' + color;

            let Elements = document.getElementsByClassName(className);

            //light up all elements with the same classname
            for (let i = 0; i < Elements.length; i++) {
                Elements[i].style.backgroundColor = hexToRgbA('#' + color, 0.3);
                Elements[i].style.borderRadius = '25px';
            }
        }

        let slice = this.state.slice.firstChild.id;
        let selector = 'div#' + slice + ' > div#slice-parent > .card' + this.state.color;
        if (document.querySelector(selector) !== null) {
            let item = document.querySelector(selector);
            item.style.borderColor = '#' + this.state.color;
            item.classList.remove('fadeOut');
            item.classList.add('fadeIn');
        }
        if (this.state.slice !== this.state.oldSlice && this.state.oldSlice !== '') {
            slice = this.state.oldSlice.firstChild.id;
            let selector = 'div#' + slice + ' > div#slice-parent > .card' + this.state.color;
            if (document.querySelector(selector) !== null) {
                let item = document.querySelector(selector);
                item.style.borderColor = '#' + this.state.color;
                item.classList.remove('fadeIn');
                item.classList.add('fadeOut');
            }
        }
    }

    /**
     * Removes all highlights
     */
    removeBackground() {
        let color = this.state.color;
        if (color !== undefined && color !== '') {
            let className = 'card' + color;
            let elements = document.getElementsByClassName(className);
            //dim all elements with the same classname
            for (let i = 0; i < elements.length; i++) {
                elements[i].style.background = 'none';
                elements[i].classList.remove('fadeIn')
            }
        }
    }

    /**
     * Called when mouseup is true
     *
     * @param e The mouseup event.
     */
    stop(e) {
        if (this.state.dragged) {
            let card = this.state.card;
            card.style.transform = 'none';
            card.style.width = 98 + '%';
            this.setState({ card: card, pickup: true });
            let slice = this.state.slice;
            if (slice.id !== undefined) {
                slice.querySelector('#' + this.state.slicePart.id).appendChild(this.state.card);
                updateStoryPoints();
                this.setState({ xOffset: this.state.currentX, yOffset: this.state.currentY, oldSlice: this.state.slice });
                this.removeEvents();
                this.removeBackground(e);
                document.getElementById('btn-card-deck-show').style.display = 'flex';
                document.getElementById('btn-card-deck-redo').style.display = 'none';
            } else {
                this.backToDeck()
            }
        } else {
            this.backToDeck()
        }
    }

    /**
     * Appends the card to the card deck.
     */
    backToDeck() {
        let selector = 'div#board-' + this.state.color;
        let card = this.state.card;
        card.style = '';
        document.querySelector(selector).appendChild(card);
        document.getElementById('btn-card-deck-show').style.display = 'flex';
        document.getElementById('btn-card-deck-redo').style.display = 'none';
        updateStoryPoints();
        this.removeBackground();
        this.removeEvents();
        this.resetState();
    }

    /**
     * Clean up the event listeners for the cards.
     * This is due to the dragging animation.
     */
    removeEvents() {
        this.state.card.removeEventListener('mousemove', this.drag);
        this.state.card.removeEventListener('mouseup', this.stop);
        this.state.card.removeEventListener('mouseleave', this.redo);
        this.state.card.removeEventListener('mouseleave', this.leaveCard);
        document.body.removeEventListener('mouseleave', this.leave);
        document.body.removeEventListener('mousemove', this.offCard)
    }

    /**
     * Reset the card back to it's original state.
     * The dragged card will be invisible when this is called.
     */
    resetState() {
        this.setState({
            slice: '',
            slicePart: '',
            card: '',
            initialX: 0,
            initialY: 0,
            xOffset: 0,
            yOffset: 0,
            pickup: false,
            oldX: 0,
            color: '',
            oldSlice: '',
            dragged: false,
        })
    }

    /**
     * Sets the x and y of the card you are dragging.
     *
     * @param e The mousemove event.
     */
    setCoords(e) {
        this.setState({
            currentX: (e.clientX - this.state.card.clientWidth / 2),
            currentY: (e.clientY - this.state.card.clientHeight / 4)
        });

        //Sets the direction of the card
        let xDirection;
        if (this.state.oldX + 1 < e.pageX) {
            xDirection = 'right';
        } else {
            xDirection = 'left';
        }

        this.setState({ dragged: true, oldX: e.pageX });
        setTranslate(this.state.currentX, this.state.currentY, this.state.card, xDirection)
    }

    render() {
        return (<div onMouseDown={this.click}>
            <div key={this.props.id} id={this.props.id} className={this.props.className}
                style={this.props.style} storypoints={this.props.storypoints} colour={this.props.colour}>
                {this.props.children}
            </div>
        </div>);
    }
}

/**
 * Loop through a board to find the cards laying in there.
 * This method is used to calculate all storypoints from a board.
 * The div holding the storypoints from that board will be instantly updated.
 * Called every time a card action has happened.
 */
function updateStoryPoints() {
    let storypoints = 0;
    //Loop through all slices
    for (let i = 0; i < document.getElementById('board').firstChild.children.length; i++) {
        let slice = document.getElementById('slice-' + i);
        let sliceNumb = i;
        //loop through all slice columns
        for (let i = 1; i < slice.childNodes[1].children.length - 1; i++) {
            let slicePartChildren = slice.querySelector('#slice-part-' + i).children;

            //counts all the cards in a slice
            let slicePartColour = slice.querySelector('#slice-part-' + i).getAttribute('columnColour');
            let elementId = slicePartColour + '-number-' + sliceNumb;
            let element = document.getElementById(elementId);
            if (element != null) {
                let cardAmount = slice.children[1].children[i].childElementCount.toString();
                element.innerHTML = cardAmount + ' Card' + (cardAmount === '1' ? '' : 's');
            }

            //loop through all slice column children
            for (let i = 0; i < slicePartChildren.length; i++) {
                if (!isNaN(Number(slicePartChildren[i].getAttribute('storypoints')))) {
                    storypoints += Number(slicePartChildren[i].getAttribute('storypoints'));
                }
            }
        }
        let sliceID = 'slice-' + i + '-storypoints';
        let e = document.getElementById(sliceID);
        if (e !== null) {
            e.style.color = 'white';
            e.style.marginLeft = '20px';
            storypoints !== 0 ? e.innerHTML = '' +
                '<i class=\'fas fa-award\'/>' +
                ' Storypoints: ' + storypoints : document.getElementById(sliceID).innerText = '';
            storypoints = 0;
        }

    }
}

/**
 * Adds the x and y to the style of the card
 * This makes the card visibly move
 */
function setTranslate(xPos, yPos, card, direction) {
    if (direction === 'right') {
        card.style.transform = 'translate3d(' + xPos + 'px, ' + yPos + 'px, 0) rotateZ(8deg)';
        card.style.MozTransform = 'translate3d(' + xPos + 'px, ' + yPos + 'px, 0) rotateZ(8deg)';
        card.style.webkitTransform = 'translate3d(' + xPos + 'px, ' + yPos + 'px, 0) rotateZ(8deg)';
    } else {
        card.style.transform = 'translate3d(' + xPos + 'px, ' + yPos + 'px, 0) rotateZ(-8deg)';
        card.style.MozTransform = 'translate3d(' + xPos + 'px, ' + yPos + 'px, 0) rotateZ(-8deg)';
        card.style.webkitTransform = 'translate3d(' + xPos + 'px, ' + yPos + 'px, 0) rotateZ(-8deg)';
    }
}

/**
 * @param hex The hex value of your color.
 * @param alpha The alpha value of your color
 * @returns {string} An rgb value from the given hex color.
 * @throws Error when an invalid hex value has been given.
 */
function hexToRgbA(hex, alpha) {
    let c;
    if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
        c = hex.substring(1).split('');
        if (c.length === 3) {
            c = [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c = '0x' + c.join('');
        return 'rgba(' + [(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',') + ',' + alpha + ')';
    }
    throw new Error('Bad Hex');
}
