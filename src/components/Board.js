import React from 'react'
import $ from 'jquery'
import DroppableSlice from './DroppableSlice'
import queryString from 'query-string'
import Timer, { startTimer } from './Timer'
import Card from './Card'
import Slice, { sliceCounter } from './Slice'
import BoardHeader from './BoardHeader'

import '../style/general.css'
import '../style/cards.css'
import '../style/cardDeck.css'
import '../style/note.css'
import '../style/slice.css'
import '../style/header.css'
import '../style/timer.css'
import '../style/persona.css'
import { loadCardData } from './CardUtil'

// This card set will be dynamically loaded inside the constructor of the card component.
let cardData = null;

export default class Board extends React.Component {

    constructor(props) {
        super(props);
        this.state = { timer: false };
        this.showCardDeck = this.showCardDeck.bind(this);
        this.closeCardDeck = this.closeCardDeck.bind(this);
        this.closeOnEsc = this.closeOnEsc.bind(this);
        this.handlePlaceholder();
        cardData = loadCardData(this.props);
        this.ref = React.createRef()
    }

    loadCardSet() {
        let cardSetName = 'undefined';
        if (this.props.location !== undefined) {
            let parsedUrl = queryString.parse(this.props.location.search);
            cardSetName = parsedUrl['gameCards'];
        }
        if (typeof cardSetName === 'undefined') {
            // No card set was given, use the default one.
            cardData = require('../data/cardSets.json');
            return;
        }

        try {
            cardData = require('../data/' + cardSetName + '.json');
        } catch (ex) {
            // Card set did not exist, use the default card set.
            cardData = require('../data/cardSets.json');
        }
    }

    /**
     * Call the method in SLice
     * Adds a slice to the board
     */
    sliceCounter() {
        sliceCounter();
    }

    /**
     * removes the placeholder if the user clicks on it.
     */
    handlePlaceholder = () => {
        $(document).on('focus', '.placeholder-owner', function () {
            $(this).removeAttr('placeholder');
        });

        $(document).on('focusout', '.placeholder-owner', function () {
            $(this).attr('placeholder', $(this).attr('textplaceholder'));
        })
    };

    /**
     * Show card deck and start timer
     */
    showCardDeck() {
        this.deckOverlay.style.visibility = 'visible';
        this.deckOverlay.style.overflowY = 'visible';
        this.btnCardDeckClose.style.display = 'flex';
        this.btnCardDeckShow.style.display = 'none';
        if (this.state.timer === false) {
            startTimer();
            this.setState({ timer: true });
        }
        document.body.style.overflow = 'hidden'
    }

    /**
     * Closes card deck
     */
    closeCardDeck() {
        this.deckOverlay.style.visibility = 'hidden';
        this.btnCardDeckShow.style.display = 'flex';
        this.btnCardDeckClose.style.display = 'none';
        document.body.style.overflow = 'auto'
    }

    /**
     * Closes card deck when esc is pressed
     */
    closeOnEsc(e) {
        if (e.key === 'Escape') {
            this.closeCardDeck();
        }
    }

    //render the board
    render() {
        return (
            <div className={'no-select'} draggable={false}>
                <div id='wrapper' onKeyDown={this.closeOnEsc} tabIndex={0} className={'purple-background'}>
                    <div id='deck-overlay' ref={input => this.deckOverlay = input} onKeyDown={this.closeOnEsc} tabIndex={0}>
                        <div id='deck-overlay-single' />
                        <Card />
                    </div>
                    <div id='persona-img-container'>
                        <Timer />
                    </div>
                    <BoardHeader />
                    <DroppableSlice id='board' className='board2'>
                        <Slice />
                    </DroppableSlice>
                    <div className='board-end-background'>
                        <i id='btn-add-slice' onClick={this.sliceCounter} className='btn-add-slice fas fa-plus fa-2x' />
                    </div>
                    <DroppableSlice>
                        <i id='btn-card-deck-show' style={{ padding: 20 + 'px' }} ref={input => this.btnCardDeckShow = input} onClick={this.showCardDeck}
                            className='fas fa-layer-group float-button'
                        />
                    </DroppableSlice>
                    <i id='btn-card-deck-close' ref={input => this.btnCardDeckClose = input} onClick={this.closeCardDeck} className='fas fa-times float-button' />
                    <i id='btn-card-deck-redo' className='fas fas fa-redo float-button' />
                </div>
            </div>
        );
    }
}

export { cardData }